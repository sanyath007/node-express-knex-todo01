const express = require('express');
const router = express.Router();
const db = require('../../database');

router.get('/', (req, res) => {
    db.select()
    .from('todo')
    .then(data => {
        res.send(data);
    });
});

router.post('/', (req, res) => {
    let { title, is_done } = req.body;

    db.insert({ title, is_done })
    .into('todo')
    .then(_id => {
        if (_id) {
            db('todo')
            .where({ id: _id})
            .then(data => {
                res.send(data);
            });
        }
    });
});

router.put('/:id', (req, res) => {
    db('todo')
    .where({ id: req.params.id })
    .update(req.body)
    .then(rows => {
        if (rows > 0) {
            db('todo')
            .where({ id: req.params.id })
            .then(data => {
                res.send(data);
            });
        }
    })
    .catch(err => console.log(err));
});

router.delete('/:id', (req, res) => {
    db('todo')
    .where({ id: req.params.id })
    .del()
    .then(() => {
        res.json({ success: true })
    });
});

router.get('/:id', (req, res) => {
    db('todo')
    .where({ id: req.params.id })
    .select()
    .then(data => res.send(data));
});

module.exports = router;
